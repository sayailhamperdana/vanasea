/*!
    * Start Bootstrap - Creative v6.0.3 (https://startbootstrap.com/themes/creative)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-creative/blob/master/LICENSE)
    */
    



    (function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 0)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
    
  });

  //
  $('.customer-logos').slick({
        slidesToShow: 20,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 2
            }
        }]
    }); 

  // Open Pop Up
  $('.menu-btn').click(function(){
    $('.menu-container').fadeIn();
  });

  $('.close-btn').click(function(){
    $('.menu-container').css('display','none');
  });

  $('.menu-item').click(function(){
    $('.menu-container').css('display','none');
  }); 

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 0
  });



  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-scrolled");
    } else {
      $("#mainNav").removeClass("navbar-scrolled");
    }
    if ($("#mainNav").offset().top > 4000) {
      $(".totop").show();
    } else {
      $(".totop").hide();
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  // Collapse Navbar
  var scrollFunc = function() {
    if($(window).width() > 720){
      if ($(".logo-sq").offset().top > 1800 && $(".logo-sq").offset().top < 2700 || $(".logo-sq").offset().top > 4100 && $(".logo-sq").offset().top < 4900 ) {
        $(".logo-sq").addClass("green");
        $(".menu-btn").addClass("green");
      } else {
        $(".logo-sq").removeClass("green");
        $(".menu-btn").removeClass("green");
      }
      if ($(".menu-btn").offset().top > 4900) {
        $(".navbar-scrolled .logo-sq").css('display','none !important');
        $(".navbar-brand.js-scroll-trigger.logo-footer").show();
      }
      else{
        $(".navbar-brand.js-scroll-trigger.logo-sq").show();
        $(".navbar-brand.js-scroll-trigger.logo-footer").hide();
      }
    }
    else{
      if ($(".logo-sq").offset().top > 10 && $(".logo-sq").offset().top < 1800 || $(".logo-sq").offset().top > 4500 && $(".logo-sq").offset().top < 5000 ) {
          $(".logo-sq").addClass("green");
          $(".menu-btn").addClass("green");
        } else {
          $(".logo-sq").removeClass("green");
          $(".menu-btn").removeClass("green");
        }
    }
  };
  // Collapse now if page is not at top
  scrollFunc();
  // Collapse the navbar when page is scrolled
  $(window).scroll(scrollFunc);

  // 

  var bgscroll = function() {
    // if ($(".logo-sq").offset().top > 760 ) {
    //   if($(".logo-sq").offset().top > 1750 ){
    //     if($(".logo-sq").offset().top > 4800 ){
    //        $("#page-top").css({"background-color": "#40E0D0", "animation-name": "wg" , "animation-duration" : "2s"});
    //     }
    //     else{
    //       $("#page-top").css({"background-color": "#fff", "animation-name": "gw" , "animation-duration" : "2s"});
    //     }
    //   }
    //   else if($(".logo-sq").offset().top > 4800 ){
    //     $("#page-top").css({"background-color": "#fde803", "animation-name": "wy" , "animation-duration" : "2s"});
    //   }
    //   else{
    //      $("#page-top").css({"background-color": "#fde803", "animation-name": "gy" , "animation-duration" : "2s"});
    //   }
    // } 
    // else{
    //     $("#page-top").css({"background-color": "#40E0D0", "animation-name": "yg" , "animation-duration" : "2s"});
    //   }
    

    // else {
    //   $("#page-top").css({"background-color": "#40E0D0", "animation-name": "wg" , "animation-duration" : "2s"});
    // }

  };
  // Collapse now if page is not at top
  bgscroll();
  // Collapse the navbar when page is scrolled
  $(window).scroll(bgscroll);

  // Magnific popup calls
  // $('#portfolio').magnificPopup({
  //   delegate: 'a',
  //   type: 'image',
  //   tLoading: 'Loading image #%curr%...',
  //   mainClass: 'mfp-img-mobile',
  //   gallery: {
  //     enabled: true,
  //     navigateByImgClick: true,
  //     preload: [0, 1]
  //   },
  //   image: {
  //     tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
  //   }
  // });


})(jQuery); // End of use strict


// var $document = $(document),
//     $element = $('#about'),
//     className = 'hasScrolled';

// $document.scroll(function() {
//   if ($document.scrollTop() >= 50) {
//     // user scrolled 50 pixels or more;
//     // do stuff
//     $element.addClass(className);
//   } else {
//     $element.removeClass(className);
//   }
// });

$("#left-arrow").click(function(){
  if($(".lf-l").hasClass("showthis")){
    $(".lf-l").fadeOut();
    $(".lf-l").removeClass("showthis");
    // alert("remove");
  }
  else{
    $(".lf-l").fadeIn();
    $(".lf-l").addClass("showthis");

    // alert("add");
  }
});

$("#right-arrow").click(function(){
  if($(".lf-r").hasClass("showthis")){
    $(".lf-r").fadeOut();
    $(".lf-r").removeClass("showthis");
    // alert("remove");
  }
  else{
    $(".lf-r").fadeIn();
    $(".lf-r").addClass("showthis");

    // alert("add");
  }
});

$(".youtube-link").grtyoutube();

$(function() {
  AOS.init({disable: 'mobile'});
});



$( document ).ready(function() {
  $.fn.isInViewport = function() {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};
      $(window).on('resize scroll', function() {
          if($(window).width() > 720){
            if ($('.i-y').isInViewport()) {
              $("#page-top").css({"background": "#fde803", "transition-property": "background" , "transition-duration" : "3s"});
            }
            else if ($('.i-w').isInViewport() || $('.i-w2').isInViewport() ) {
              $("#page-top").css({"background": "#fff", "transition-property": "background" , "transition-duration" : "3s"});
            }
            else if ($('.i-g').isInViewport() || $('.i-g2').isInViewport() ) {
              $("#page-top").css({"background": "#40E0D0", "transition-property": "background" , "transition-duration" : "2s"});
            }
          }
      });


});