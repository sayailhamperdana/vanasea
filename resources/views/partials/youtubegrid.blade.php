<!-- Portfolio-->
        <div id="portfolio">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-lg-4 col-sm-6 d-flex align-items-center" data-aos="zoom-in" data-aos-duration="2000">
                        <!-- <a class="portfolio-box" href="assets/img/portfolio/fullsize/1.jpg">
                            
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a> -->
                        <h1 class="big-title mt-0 p-3">Work.</h1>
                    </div>
                    <div class="col-lg-4 col-sm-6" data-aos="zoom-in" data-aos-duration="2000">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/2.jpg">
                            <img class="img-fluid" src="assets/img/r1.png" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name"><i class="fab fa-youtube"></i></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6" data-aos="zoom-in" data-aos-duration="2000">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/3.jpg">
                            <img class="img-fluid" src="assets/img/r2.png" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name"><i class="fab fa-youtube"></i></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6" data-aos="zoom-in" data-aos-duration="2000">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/4.jpg">
                            <img class="img-fluid" src="assets/img/r3.png" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name"><i class="fab fa-youtube"></i></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6" data-aos="zoom-in" data-aos-duration="2000">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/5.jpg">
                            <img class="img-fluid" src="assets/img/r4.png" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name"><i class="fab fa-youtube"></i></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6" data-aos="zoom-in" data-aos-duration="2000">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/6.jpg">
                            <img class="img-fluid" src="assets/img/r5.png" alt="" />
                            <div class="portfolio-box-caption p-3">
                                <div class="project-name"><i class="fab fa-youtube"></i></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6" data-aos="zoom-in" data-aos-duration="2000">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/1.jpg">
                            <img class="img-fluid" src="assets/img/r6.png" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name"><i class="fab fa-youtube"></i></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6" data-aos="zoom-in" data-aos-duration="2000">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/2.jpg">
                            <img class="img-fluid" src="assets/img/r7.png" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name"><i class="fab fa-youtube"></i></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6" data-aos="zoom-in" data-aos-duration="2000">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/3.jpg">
                            <img class="img-fluid" src="assets/img/r8.png" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name"><i class="fab fa-youtube"></i></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6" data-aos="zoom-in" data-aos-duration="2000">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/4.jpg">
                            <img class="img-fluid" src="assets/img/r9.png" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name"><i class="fab fa-youtube"></i></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6" data-aos="zoom-in" data-aos-duration="2000">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/5.jpg">
                            <img class="img-fluid" src="assets/img/r10.png" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name"><i class="fab fa-youtube"></i></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6 d-flex align-items-center" data-aos="zoom-in" data-aos-duration="2000">
                        <!-- <a class="portfolio-box" href="assets/img/portfolio/fullsize/1.jpg">
                            
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a> -->
                        <h1 class="big-title mt-0 p-3" style="line-height: 130px;">See More <i style="color: #40E0D0; font-size:100px;" class="fas fa-arrow-right"></i></h1>
                    </div>
                </div>
            </div>
        </div>