 <!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light py-3" id="mainNav">
    <div class="container">
	<a class="navbar-brand " href="/"><img style="height: 50px;
	margin-top: 10px; margin-left: -50px;" src="{{ asset('assets/img/logo-color.svg') }}"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <!-- <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#intro">Intro</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">What</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#portfolio">Work</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#findus">Find Us</a></li>
            </ul> -->
        </div>
    </div>
    <div class="bar-btn menu-btn" style="padding: 5px; margin-right: 10px;">
        <img style="width: 40px; margin-right: 20px;" src="{{ asset('assets/img/bar-color.png') }}">
    </div>
</nav>