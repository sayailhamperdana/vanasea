 <!-- Footer-->
<footer class="h-100 py-5" style="background: #40E0D0; color: #fff;" id="findus">
    <div class="container">
        <div class="row">
            <div class="col-md-12 d-flex justify-content-end" style="margin-bottom: 50px;">
                <ul class="list-group list-group-horizontal no-bg-bd">
                  <li class="list-group-item">Contact Us</li>
                  <li class="list-group-item">About Us</li>
                  <li class="list-group-item">Career</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <span style="font-size: 22px; margin-left: 5px;">Lets make it happen</span>
                <h1 class="big-title" style="color: #fff;">Find Us.</h1>
                <p style="width: 80%; font-size: 24px; font-weight: 400;">
                    Resort Dago Pakar - Cluster Forrest Hills Jl.Oakwood E-6, Mekarsaluyu, Cimenyan, Bandung, West Java 40198
                </p>
                <span style="font-size: 24px; font-weight: 900; font-family: 'Metropolis-Black';">(022) 2506810</span>
                <div class="pt-2 d-flex justify-content-start">
                    <ul class="list-group list-group-horizontal no-bg-bd sosial-btn">
                      <li class="list-group-item"><img src="{{ asset('assets/img/tw.png') }}"></li>
                      <li class="list-group-item"><img src="{{ asset('assets/img/ig.png') }}"></li>
                      <li class="list-group-item"><img src="{{ asset('assets/img/yt.png') }}"></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="" style="margin-top:70px;">
                    <h1 class="med-title" style="margin-bottom: 20px; color: #fff; margin-left: -50px;">#infovanasea</h1>
                    <img style="width: 580px; margin-left: -200px;" src="{{ asset('assets/img/laptop.png') }}">
                </div>
            </div>
        </div>
        <div class="row pt-2" style="margin-top:50px; border-top: solid 1px #fff;">
            <div class="col-md-6 justify-content-start d-flex align-items-center">
                <span>Copyright 2020 © Vanasea All Right Reserved</span>
            </div>
            <div class="col-md-6">
                <div class="d-flex justify-content-end align-items-center">
                    <ul class="list-group list-group-horizontal no-bg-bd">
                      <li class="list-group-item">TERM OF US</li>
                      <li class="list-group-item">PRIVACY POLICY</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="small text-center text-muted"> </div>
    </div>
</footer>