<!-- Pop Up -->

<div class="menu-container h-100">
   
        <div class="row" style="padding: 30px 40px;">
            <div class="col-6 col-md-6 col-xs-6">
                <img class="logo-popup" src="{{ asset('assets/img/logo-white.png') }}">
            </div>
            <div class="col-6 col-md-6 col-xs-6 d-flex justify-content-end close-btn">
                <i style="color: #fff; font-size: 40px;" class="fas fa-times"></i>
            </div>
        </div>
        <div class="container">
            <div class="row menu-desk">
                <div class="col-md-6  p-3">
                    <div class="sosial-menu" style="margin-left: -50px;">
                        <h2 class="text-white p-3" style="font-weight: 600; margin-bottom: 50px;">Social Media</h2>
                        <ul class="list-group no-bg-bd" style="color: #fff;">
                          <li class="list-group-item">
                              <div class="row">
                                <div class="col-md-2">
                                    <img src="{{ asset('assets/img/ig.png') }}">
                                </div>
                                <div class="col-md-10">
                                    <a href="https://www.instagram.com/vanasea.id/" target="blank" style="text-decoration: none; color: #fff;"><h3>Instagram</h3></a>
                                </div>
                              </div>
                          </li>
                          <li class="list-group-item">
                              <div class="row">
                                <div class="col-md-2">
                                    <img src="{{ asset('assets/img/yt.png') }}">
                                </div>
                                <div class="col-md-10">
                                    <a href="https://www.youtube.com/channel/UCvIqTh9dPx4UYYkyKeXtoiw" target="blank" style="text-decoration: none; color: #fff;"><h3>Youtube</h3></a>
                                </div>
                              </div>
                          </li>
                          <li class="list-group-item">
                              <div class="row">
                                <div class="col-md-2">
                                    <img src="{{ asset('assets/img/tw.png') }}">
                                </div>
                                <div class="col-md-10">
                                    <a href="https://twitter.com/VanaseaArtwork" target="blank" style="text-decoration: none; color: #fff;"><h3>Twitter</h3></a>
                                </div>
                              </div>
                          </li>
                        </ul>
                    </div>
                    <div class="footer-menu text-white" style="margin-left: -40px; margin-top:50px;">
                        <h3 style="font-family: 'Metropolis-Black' !important;">OUR OFFICE</h3>
                        <p>Resort Dago Pakar - Cluster Forrest Hills Jl.Oakwood E-6 :, Mekarsaluyu, Cimenyan, Bandung, West Java 40198
                        </p>
                    </div>
                </div>
                <div class="col-md-6" style="margin-top: -80px;">
                     <h2 class="text-white p-3" style="font-weight: 600; margin-bottom: 50px;">Website Menu</h2>
                     <ul class="list-group no-bg-bd menu-list" style="color: #fff;">
                          <li class="list-group-item">
                              <a href="#intro" class="js-scroll-trigger menu-item">Intro</a>
                          </li>
                          <li class="list-group-item">
                              <a href="#about" class="js-scroll-trigger menu-item">What</a>
                          </li>
                          <li class="list-group-item">
                              <a href="#portfolio" class="js-scroll-trigger menu-item">Work</a>
                          </li>
                          <li class="list-group-item">
                              <a href="#findus" class="js-scroll-trigger menu-item">Find Us</a>
                          </li>
                        </ul>
                </div>
            </div>
             <div class="menu-mobile">
              <h6 class="text-white p-3" style="font-weight: 600;">Website Menu</h6>
              <ul class="list-group no-bg-bd" style="color: #fff; margin-bottom: 30px;">
                  <li class="list-group-item">
                      <a href="#intro" class="js-scroll-trigger menu-item">Intro</a>
                  </li>
                  <li class="list-group-item">
                      <a href="#about" class="js-scroll-trigger menu-item">What</a>
                  </li>
                  <li class="list-group-item">
                      <a href="#portfolio" class="js-scroll-trigger menu-item">Work</a>
                  </li>
                  <li class="list-group-item">
                      <a href="#findus" class="js-scroll-trigger menu-item">Find Us</a>
                  </li>
                </ul>
                <h6 class="text-white p-3" style="font-weight: 600;">Social Media</h6>
                <ul class="list-group no-bg-bd sosmed-mobile" style="color: #fff;">
                          <li class="list-group-item">
                              <div class="row">
                                <div class="col-xs-4">
                                    <img src="{{ asset('assets/img/ig.png') }}">
                                </div>
                                <div class="col-xs-8">
                                    <a href="https://www.instagram.com/vanasea.id/" target="blank" style="text-decoration: none; color: #fff;"><h3>Instagram</h3></a>
                                </div>
                              </div>
                          </li>
                          <li class="list-group-item">
                              <div class="row">
                                <div class="col-xs-4">
                                    <img src="{{ asset('assets/img/yt.png') }}">
                                </div>
                                <div class="col-xs-8">
                                    <a href="https://www.youtube.com/channel/UCvIqTh9dPx4UYYkyKeXtoiw" target="blank" style="text-decoration: none; color: #fff;"><h3>Youtube</h3></a>
                                </div>
                              </div>
                          </li>
                          <li class="list-group-item">
                              <div class="row">
                                <div class="col-xs-4">
                                    <img src="{{ asset('assets/img/tw.png') }}">
                                </div>
                                <div class="col-xs-8">
                                    <a href="https://twitter.com/VanaseaArtwork" target="blank" style="text-decoration: none; color: #fff;"><h3>Twitter</h3></a>
                                </div>
                              </div>
                          </li>
                        </ul>
                        <div class="footer-menu text-white">
                        <h4 style="font-family: 'Metropolis-Black' !important;">OUR OFFICE</h4>
                        <p>Resort Dago Pakar - Cluster Forrest Hills Jl.Oakwood E-6 :, Mekarsaluyu, Cimenyan, Bandung, West Java 40198
                        </p>
                    </div>
            </div>
        </div>

       
   
</div>