 <!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container-fluid">
        <a class="navbar-brand logo-sq js-scroll-trigger" href="#page-top">
            <svg xmlns="http://www.w3.org/2000/svg" width="53.581" height="64.052" viewBox="0 0 53.581 64.052" style="height: 50px;
			margin-top: 10px;">
			<g id="Group_54" data-name="Group 54" transform="translate(-41.709 -4903.254)">
			<path id="Path_4" data-name="Path 4" d="M41.833,4911.844l9.125,35.279a3.85,3.85,0,0,0,5.55,2.429l9.081-4.874a3.853,3.853,0,0,0,2.006-3.824l-3.842-34.177a3.851,3.851,0,0,0-4.806-3.295l-14.364,3.772a3.851,3.851,0,0,0-2.75,4.69" fill="#f9f9f9"/>
			<path id="Path_5" data-name="Path 5" d="M75.264,4960.569a3.875,3.875,0,0,1-2.843,1.078,3.685,3.685,0,0,1-3.543-3.256c-.078-.849,1.178-12.812,1.178-12.812a2.99,2.99,0,0,0-2.686.083l-11.334,6.187a2.726,2.726,0,0,0-.583,4.424l11.017,10.251a2.97,2.97,0,0,0,3.981.032l11.444-10.3a2.7,2.7,0,0,0,.906-2.233Z" fill="#f9f9f9"/>
			<path id="Path_6" data-name="Path 6" d="M70.419,4957.351c-.232,2.042,1.578,3.786,3.9,2.19l9.3-8.269a6.627,6.627,0,0,0,2-3.206l9.474-34.822a2.448,2.448,0,0,0-3.235-3.265l-12.735,4.7c-3.846,1.494-4.878,3.186-5.153,6.309Z" fill="#f9f9f9"/>
			</g>
			</svg>
		</a>
        <a class="navbar-brand js-scroll-trigger logo-hz" href="#page-top"></a>
        <a class="navbar-brand js-scroll-trigger logo-footer" href="#page-top"><img style="height: 50px;
            margin-top: 10px;" src="{{ asset('assets/img/logo-white.png') }}"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <!-- <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#intro">Intro</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">What</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#portfolio">Work</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#findus">Find Us</a></li>
            </ul> -->
        </div>
    </div>
    <div class="bar-btn menu-btn" style="padding: 5px; margin-right: 10px;">
        <svg style="width: 40px; margin-right: 20px;" id="Group_8" data-name="Group 8" xmlns="http://www.w3.org/2000/svg" width="50" height="27" viewBox="0 0 50 27">
          <rect id="Rectangle_40" data-name="Rectangle 40" width="50" height="8" rx="4" fill="#fff"/>
          <rect id="Rectangle_42" data-name="Rectangle 42" width="41" height="8" rx="4" transform="translate(9 19)" fill="#fff"/>
        </svg>

    </div>
</nav>