@extends('layouts.app')

@section('content')
<div class="container h-100 pt-5">
	<div class="row" style="margin-bottom: -50px;">
		<div class="col-md-12">
			<h1 class="med-title">Contact Us</h1>
		</div>
	</div>
    <div class="row" style="margin-top: 100px">
        <div class="col-md-6">
        	<div class="mapouter"><div class="gmap_canvas"><iframe width="500" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Vanasea%20artwork&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.whatismyip-address.com/nordvpn-coupon/"></a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:500px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:500px;}</style></div>
        </div>
        <div class="col-md-6">
			<h1 class="med-title">Our Office</h1>
            <p class="address" style="width: 80%; font-size: 20px; font-weight: 400;">
                Resort Dago Pakar - Cluster Forrest Hills Jl.Oakwood E-6, Mekarsaluyu, Cimenyan, Bandung, West Java 40198
            </p>
            <span style="font-size: 24px; font-weight: 900; font-family: 'Metropolis-Black';">(022) 2506810</span>                    
        </div>
    </div>
</div>
@endsection
