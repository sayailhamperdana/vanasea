<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>VANASEA</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon.png') }}" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <!-- Third party plugin CSS-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet" />
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://unpkg.com/aos@2.3.0/dist/aos.js"></script>
        


        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="https://unpkg.com/aos@2.3.0/dist/aos.css" rel="stylesheet">
        <link href="{{ asset('css/grt-youtube-popup.css') }}" rel="stylesheet">
    </head>
        <body id="page-top">
      
       @include('partials/menu')

       @include('partials/nav')


        <!-- To TOP -->
        <div class="totop">
            <a class="js-scroll-trigger" href="#page-top"><i class="fas fa-chevron-circle-up"></i></a>
        </div>  
        <!-- Masthead-->
        <header class="masthead" id="intro">
            <div class="container-fluid h-100">
                <div class="row h-100 align-items-center justify-content-center text-center i-g">
                    <!-- <div class="col-lg-10 align-self-end">
                        <h1 class="text-uppercase text-white font-weight-bold">Your Favorite Source of Free Bootstrap Themes</h1>
                        <hr class="divider my-4" />
                    </div>
                    <div class="col-lg-8 align-self-baseline">
                        <p class="text-white-75 font-weight-light mb-5">Start Bootstrap can help you build better websites using the Bootstrap framework! Just download a theme and start customizing, no strings attached!</p>
                        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>
                    </div> -->
                    <div class="ftitle">
                        <h4>Creative Vision</h4>
                        <p>Mendarat di bumi sejak tahun 2014</p>
                    </div>
                    <div class="col-md-12 no-padding videoCon">
                        <video id="video" width="100%" height="auto" autoplay muted loop>
                          <source src="{{ asset('assets/upload/intro2.mp4') }}" type="video/mp4">
                        </video>
                    </div>

                </div>
            </div>
        </header>
        <!-- About-->
        <section class="aboutz page-section" style="" id="about">
            <div class="container about-section ">
                <div class="row i-y">
                    <div class="col-md-6">
                        <h1 class="big-title mt-0 whatTitle" data-aos="fade-up" data-aos-duration="1000">What.</h1>
                        <div class="" data-aos="fade-up" data-aos-duration="3000">
                            <img class="img-f1" style="width: 126%; margin-left: -130px; margin-top: -100px;" src="{{ asset('assets/img/image1.png') }}">
                        </div>
                    </div>
                    <div class="col-md-6 d-flex align-items-center">
                        <p class="align-middle" style="color: #fff; font-weight: 900; margin-left: 50px; margin-right: -50px;" data-aos="fade-up" data-aos-duration="3000">
                            Vanasea was established since 2014 as 
                            production house, formed by group of enenrgetic and creative people with related experiences in the field of creative and communication product.
                            <br>
                            <br>
                            We merge our clients to develop tye most effective and attractive branding product to reach their va ried targets. We work as passionately creative team to develop your spesific message, strategy and goals. 
                        </p>
                    </div>
                </div>
                <div class="row i-y">
                    <div class="col-md-4 d-flex align-items-start" data-aos="fade-up" data-aos-duration="3000">
                        <div class="">
                            <h1 class="med-title mt-0">Communication & Creative Idea </h1>
                            <p class="align-middle" style="color: #fff; font-weight: 900;">
                                Not only expert in creative concept but also 
                                in brand communication strategy. We believe 
                                that effective communication is one of the key
                                a success video marketing.
                            </p>
                             <div class="row list-fitur">
                                <div class="col-md-3" data-aos="fade-up" id="right-arrow">
                                    <i  style="color: #40E0D0; font-size: 70px;" class="fas fa-arrow-right"></i>
                                </div>
                                <div class="col-md-9">
                                    <ul class="list-group lf-r" style="display: none;">
                                      <li class="list-group-item">360 Brand Campaign</li>
                                      <li class="list-group-item">Manifesto Campaign</li>
                                      <li class="list-group-item">Company Profile</li>
                                      <li class="list-group-item">Coorporate Identity</li>
                                      <li class="list-group-item">Brand Collateral</li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex align-items-center" data-aos="fade-up" data-aos-duration="1000">
                         <img class="img-f2" style="width: 100%" src="{{ asset('assets/img/image2.png') }}">
                    </div>
                    <div class="col-md-4 d-flex align-items-end i-y" data-aos="fade-up" data-aos-duration="3000">
                        <div class="" style="text-align: right;">
                            <h1 class="med-title mt-0">Production</h1>
                            <p class="align-middle" style="color: #fff; font-weight: 900;">
                                We create a marketing content strategic 
                                for both commercial and corporate 
                                campaign.
                            </p>
                            <div class="row list-fitur i-y">
                                <div class="col-md-9">
                                    <ul class="list-group lf-l" style="margin-left: 60px; margin-right: -60px; display: none;">
                                      <li class="list-group-item">TV Commercial Ads</li>
                                      <li class="list-group-item">Digital Commercial Ads</li>
                                      <li class="list-group-item">Webseries</li>
                                      <li class="list-group-item">Coorporate Video</li>
                                      <li class="list-group-item">Company Profile Video</li>
                                      <li class="list-group-item">2D/3D Video</li>
                                    </ul>
                                </div>
                                <div class="col-md-3" id="left-arrow">
                                    <i  style="color: #40E0D0; font-size: 70px;" class="fas fa-arrow-left"></i>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About 2-->
     
        <!-- Portfolio-->
        <div id="portfolio" class="i-w">
            <div class="workz container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-lg-4 col-sm-6 d-flex align-items-center" data-aos="zoom-in" data-aos-duration="2000">
                        <!-- <a class="portfolio-box" href="assets/img/portfolio/fullsize/1.jpg">
                            
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a> -->
                        <h1 class="big-title mt-0 p-3">Work.</h1>
                    </div>
                    @foreach ($limitrs as $pi)
                        <div class="col-lg-4 col-sm-6 col-xs-6 persen50" data-aos="zoom-in" data-aos-duration="2000">
                            <a class="portfolio-box youtube-link" youtubeid="{{ $pi->contentDetails->videoId }}">
                                <img class="img-fluid" src="{{ $pi->snippet->thumbnails->standard->url }}" alt="" />
                                <div class="portfolio-box-caption">
                                    <div class="project-category text-white-50">{{ $pi->snippet->channelTitle }}</div>
                                    <div class="project-name">{{ $pi->snippet->title }}</i></div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                    
                    <div class="col-lg-4 col-sm-6 d-flex align-items-center" data-aos="zoom-in" data-aos-duration="2000">
                        <!-- <a class="portfolio-box" href="assets/img/portfolio/fullsize/1.jpg">
                            
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a> -->
                        <h1 class="big-title mt-0 p-3 seemore" style="line-height: 130px;"><a href="https://www.youtube.com/playlist?list=PL_pMHU_Kdji0yg5kNWaSRv4G953Ad3brC" style="text-decoration: none;color: #40E0D0;">See More </a><i style="color: #40E0D0; font-size:100px;" class="fas fa-arrow-right"></i></h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- Call to action-->
        <section class="page-section clientz">
            <div class="container text-center">
                <!-- <h2 class="mb-4">Free Download at Start Bootstrap!</h2>
                <a class="btn btn-light btn-xl" href="https://startbootstrap.com/themes/creative/">Download Now!</a> -->
                <h1 class="big-title mt-0 p-3" style="color: #454545;" data-aos="fade-up" data-aos-duration="3000">Clients.</h1>
                <div class="client-section i-w2">
                    <table class="client-list">
                        <tr>
                            <td><img src="{{ asset('assets/img/logo1.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo2.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo3.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo4.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo5.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('assets/img/logo6.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo7.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo8.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo9.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo10.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('assets/img/logo11.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo12.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo13.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo14.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo15.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('assets/img/logo16.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo17.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo-1.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo19.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <td><img src="{{ asset('assets/img/logo20.png') }}" data-aos="zoom-in" data-aos-duration="1000"></td>
                            <!-- <td><img src="{{ asset('assets/img/logo19.png') }}"></td>
                            <td><img src="{{ asset('assets/img/logo20.png') }}"></td> -->
                        </tr>
                    </table>
                </div>
                <div class="customer-logos slider">
                  <div class="slide"><img src="{{ asset('assets/img/logo1.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo2.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo3.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo4.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo5.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo6.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo7.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo8.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo9.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo10.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo11.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo12.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo13.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo14.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo15.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo16.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo17.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo-1.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo19.png') }}"></div>
                  <div class="slide"><img src="{{ asset('assets/img/logo20.png') }}"></div>
               </div>
            </div>
        </section>
        <!-- Contact-->
         <!-- Footer-->
        <footer class="i-g2" style="background: transparent; color: #fff;" id="findus">
            <div class="container ">
                <!-- <div class="row ">
                    <div class="col-md-12 d-flex justify-content-end" style="margin-bottom: 50px;">
                        <ul class="list-group list-group-horizontal no-bg-bd">
                          <li class="list-group-item"><a href="/contact" style="text-decoration: none;">Contact Us</a></li>
                          <li class="list-group-item"><a href="/about" style="text-decoration: none;">About Us</a></li>
                          <li class="list-group-item"><a href="/career" style="text-decoration: none;">Career</a></li>
                        </ul>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-md-8">
                        <span style="font-size: 22px; margin-left: 5px;">Lets make it happen</span>
                        <h1 class="big-title" style="color: #fff;">Find Us.</h1>
                        <p class="address" style="width: 80%; font-size: 24px; font-weight: 400;">
                            <a href="https://goo.gl/maps/hhQRfmJRFby42F7F7" style="text-decoration: none;color: #fff;">Resort Dago Pakar - Cluster Forrest Hills Jl.Oakwood E-6, Mekarsaluyu, Cimenyan, Bandung, West Java 40198</a>
                        </p>
                        <span style="font-size: 24px; font-weight: 900; font-family: 'Metropolis-Black';">(022) 2506810</span>
                        <div class="pt-2 d-flex justify-content-start">
                            <ul class="list-group list-group-horizontal no-bg-bd sosial-btn">
                              <li class="list-group-item"><a href="https://www.youtube.com/channel/UCvIqTh9dPx4UYYkyKeXtoiw" target="blank"><img src="{{ asset('assets/img/tw.png') }}"></a></li>
                              <li class="list-group-item"><a href="https://www.instagram.com/vanasea.id/" target="blank"><img src="{{ asset('assets/img/ig.png') }}"></a></li>
                              <li class="list-group-item"><a href="https://www.youtube.com/channel/UCvIqTh9dPx4UYYkyKeXtoiw" target="blank"><img src="{{ asset('assets/img/yt.png') }}"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="" style="margin-top:70px;">
                            <h1 class="med-title" style="margin-bottom: 20px; color: #fff; margin-left: -50px;">#infovanasea</h1>
                            <a href="http://instagram.com/vanasea.id"><img data-aos="fade-up" data-aos-duration="3000" class="img-f3" style="width: 580px; margin-left: -200px;" src="{{ asset('assets/img/laptop.png') }}"></a>
                        </div>
                    </div>
                </div>
                <div class="row pt-2" style="margin-top:50px; border-top: solid 1px #fff;">
                    <div class="col-md-6 justify-content-start d-flex align-items-center">
                        <span>Copyright 2020 © Vanasea All Right Reserved</span>
                    </div>
                    <div class="col-md-6">
                        <div class="d-flex justify-content-end align-items-center">
                            <ul class="list-group list-group-horizontal no-bg-bd">
                              <li class="list-group-item">TERM OF US</li>
                              <li class="list-group-item">PRIVACY POLICY</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="small text-center text-muted"> </div>
            </div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{ asset('js/grt-youtube-popup.js') }}"></script>
        <script src="{{ asset('js/scripts.js') }}"></script>

    </body>
</html>
