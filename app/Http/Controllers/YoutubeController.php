<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alaouy\Youtube\Facades\Youtube;
use App\MetaData;


class YoutubeController extends Controller
{
    public function index()
    {
    	$videoLink = MetaData::where('metadata_name','=','youtube')->get();
    	$link = '';
    	foreach ($videoLink as $yl ) {
    		$link = $yl->value;
    		
    	}
    	$playlistItems = Youtube::getPlaylistItemsByPlaylistId($link);
        $results = $playlistItems["results"];
        $limitrs = array_slice($results,0,10,true);
        return view('welcome',['limitrs'=>$limitrs]);
    }
}
